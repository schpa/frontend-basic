var path = require('path');
var webpack = require('webpack');
var HtmlWebPackPlugin = require('html-webpack-plugin');
var CompressionPlugin = require('compression-webpack-plugin');

//
var websocketAddress = process.env.MONITOR_WS
var version = process.env.MONITOR_GITSHA
var notes = process.env.MONITOR_NOTES
//

var TARGET = process.env.npm_lifecycle_event;

if (TARGET === 'start') {
    console.log("running app in development")
}

var destination = process.env.DEST_DIR
var PRODUCTION = process.env.NODE_ENV === 'production';

var plugins = PRODUCTION ? [
    new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
    }),
    uglify(),
    compression(),
    template()
] : [
    template()
];

function compression() {
    return new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.min.js$/,
        threshold: 0,
        minRatio: 0.5,
        verbose: false
    })

}

function uglify() {
    return new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            screw_ie8: true,
            conditionals: true,
            unused: true,
            comparisons: true,
            sequences: true,
            dead_code: true,
            evaluate: true,
            if_return: true,
            join_vars: true,
        },
        output: {
            comments: false
        },
    })
}

function template() {
    return new HtmlWebPackPlugin({
        template: './src/index-template.html',
        metadata: {
            websocketAddress: "localhost:8000",
            version: "1.0",
            notes: "for running afb-monitor"
        }
    })
}

var defaults = {
    entry: ['./src/index.js'],
    output: {
        path: PRODUCTION ? "./dist" : "/dist",
        publicPath: PRODUCTION ? "" : "",
        filename: PRODUCTION ? "app.[hash:4].min.js" : "app.js",
        pathinfo: false
    },
    resolve: {
        modules: ['node_modules'],
        extensions: ['.js'],
    },
    plugins: plugins,
    module: {
        rules: [{
            test: /\.elm$/,
            exclude: [/elm-stuff/, /node_modules/],
            include: /src/,

            loader: 'elm-webpack-loader?debug=true'
        }, {
            test: /\.scss$/,
            include: /src/,
            loader: 'style-loader!css-loader!sass-loader'
        }, {
            test: /\.(jpe?g|png|gif|svg)$/i,
            include: /src/,
            loaders: [
                'file-loader?name=[name].[ext]'
            ]
        }, {
            test: /\.(mp3)$/i,
            include: /src\/audio\//,
            loaders: [
                'file-loader?name=[name].[ext]'
            ]
        }]
    },
    performance: {
        hints: false
    }
};


module.exports = [defaults]

function here(d) {
    return d ? path.join(__dirname, d) : path.join(__dirname, "");
}

console.log(__dirname)

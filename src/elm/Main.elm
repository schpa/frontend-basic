port module Main exposing (..)

import Html exposing (Html, div, text, p, li, h1, img, ul)
import List exposing (..)
import Html.Attributes exposing (class, src, width, height)


port audioSignal : Int -> Cmd msg


list : List Int
list =
    range 1 50


header : String -> Html msg
header t =
    h1 [ class "h1" ] [ text t ]


imageContainer : String -> Html msg
imageContainer x =
    div [ class "imageContainer withMargins" ]
        [ img
            [ src x ]
            []
        ]


li : List (Html.Attribute msg) -> List (Html msg) -> Html msg
li a =
    Html.li (concat [ a, [ class "withMargins" ] ])


view : a -> Html msg
view n =
    let
        textp t =
            li [] [ text <| toString t ]
    in
        div []
            [ div [ class "contList1" ]
                [ p [] [ text "a" ]
                , p [] [ text "b" ]
                ]
            , div [ class "contList2" ]
                [ header "Title of this program"
                , imageContainer "bg.jpg"
                , div [ class "outer" ]
                    [ ul [ class "half" ] <|
                        concat
                            [ (map textp list)
                            , map
                                (\e ->
                                    li [ class "filler" ]
                                        [ text <| toString e ]
                                )
                                (range 1 19)
                            ]
                    ]
                ]
            ]


main : Program Never number msg
main =
    Html.program
        { init = 0 ! [ audioSignal 1 ]
        , view = view
        , update = \_ _ -> 0 ! []
        , subscriptions = \_ -> Sub.none
        }
